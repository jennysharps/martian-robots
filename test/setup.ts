import '../polyfills';
import chalk from 'chalk';
import thunk from 'redux-thunk';
import { expect } from 'chai';
const sinon = require('sinon');
const deepFreeze = require('deep-freeze');
const configureMockStore = require('redux-mock-store');
const sinonChai = require('sinon-chai');
const chai = require('chai');
chai.use(sinonChai);

console.log(chalk.cyan(`\n=============== Starting Test Suite =================`));

global.deepFreeze = deepFreeze;
global.expect = expect;
global.mockStore = configureMockStore([thunk]);
global.sinon = sinon;

