# martian-robots

## Usage
Prerequisite: node & yarn or npm

### To Install & Run Locally
1. Run `yarn install` in root of the application
2. Run `npm start`
3. Answer the prompts in the console

### To Run Tests
1. Run `npm test` in root of the application

## Where to Look
The code is organized in the following folders:

```
- scripts
- src
-- actions
-- components
-- interfaces
-- reducers
-- selectors
-- store
-- utils
```

## Implementation Notes
I have always heard that Redux could be used outside of React applications, so after a bit of research decided to attempt building a solution with it given the fact that having global state management would make the program more manageable. This definitely had its challenges! I incidentally ended up with a much more robust solution than was necessary to solve the problem in isolation, but with flexibility and reuse in mind.

## Assumptions
I wasn't clear on when the final robot position should be reported, so assumed it should be after each robot's journey to allow for infinite robots being sent to Mars.  As the data is in the application state, it would be trivial to change it so that a report is sent back at a different point.

## The Problem
The surface of Mars can be modelled by a rectangular grid around which robots are able to
move according to instructions provided from Earth. You are to write a program that
determines each sequence of robot positions and reports the final position of the robot.

## The Input
The first line of input is the upper-right coordinates of the rectangular world, the lower-left
coordinates are assumed to be 0, 0.

The remaining input consists of a sequence of robot positions and instructions (two lines per
robot). A position consists of two integers specifying the initial coordinates of the robot and
an orientation (N, S, E, W), all separated by whitespace on one line. A robot instruction is a
string of the letters "L", "R", and "F" on one line.

Each robot is processed sequentially, i.e., finishes executing the robot instructions before the
next robot begins execution.

The maximum value for any coordinate is 50.

All instruction strings will be less than 100 characters in length.

## The Output
For each robot position/instruction in the input, the output should indicate the final grid
position and orientation of the robot. If a robot falls off the edge of the grid the word "LOST"
should be printed after the position and orientation.

### Sample Input
```console
5 3
1 1 E
RFRFRFRF

3 2 N
FRRFLLFFRRFLL

0 3 W
LLFFFLFLFL
```

### Sample Output
```console
1 1 E
3 3 N LOST
2 3 S
```
