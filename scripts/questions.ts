import { Questions } from 'inquirer';
import { Orientation } from '../src/interfaces/location';
import { RobotInstruction } from '../src/interfaces/robots';

import { parseBounds, parseInitialPositions, parseInstructions } from '../src/utils/parsers';

export interface MarsAnswers {
  bounds: string;
}

export interface RobotAnswers {
  positions: string;
  instructions: string;
}

export const getMarsQuestions = (): Questions<MarsAnswers> => ([
  {
    type: 'input',
    name: 'bounds',
    message: `Enter Mars' bounds (top right coordinates):`,
    validate: (value = '') => {
      const [x, y] = parseBounds(value);

      if (isNaN(x) || isNaN(y)) {
        return `Please enter valid numbers for Mars's bounds ie:, 11 15`;
      }

      return true;
    },
  },
]);

export const getRobotQuestions = ({ bounds }: MarsAnswers): Questions<RobotAnswers> => ([
  {
    type: 'input',
    name: 'positions',
    message: `Enter robot's start position:`,
    validate: (value = '') => {
      const [startingX, startingY, orientation] = parseInitialPositions(value);
      if (isNaN(startingX)
        || isNaN(startingY)
        || !Object.values(Orientation).includes(orientation)
      ) {
        return `Please enter valid numbers for the robot's starting coordinates and orientation `
          + `ie:, 2 3 N`;
      }

      const [marsBoundsX, marsBoundsY] = parseBounds(bounds);
      if (startingX > marsBoundsX) {
        return `Please enter robot starting position x no greater than ${marsBoundsX}`;
      }

      if (startingY > marsBoundsY) {
        return `Please enter robot starting position y no greater than ${marsBoundsY}`;
      }

      return true;
    },
  },
  {
    type: 'input',
    name: 'instructions',
    message: 'Enter instructions:',
    validate: (value = '') => {
      const instructions = parseInstructions(value);
      if (instructions.some(instruction => (
          !Object.values(RobotInstruction).includes(instruction)))
      ) {
        return `Unknown instruction detected. Please enter any combination of the following `
          + `instructions: ${Object.values(RobotInstruction).join(', ')}`;
      }

      return true;
    },
  },
]);
