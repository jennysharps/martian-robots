const packageJson = require('../package.json');
import '../polyfills';
import chalk from 'chalk';
import * as program from 'commander';
import { prompt } from 'inquirer';

// Interfaces
import { Planet } from '../src/interfaces/application';
import { Orientation } from '../src/interfaces/location';
import { RobotInstruction } from '../src/interfaces/robots';

// Scripts & Utils
import {
  getMarsQuestions,
  getRobotQuestions,
  MarsAnswers,
  RobotAnswers,
} from './questions';
import { parseBounds, parseInitialPositions, parseInstructions } from '../src/utils/parsers';

// Components
import Mars from '../src/components/Mars';
import Robot from '../src/components/Robot';

program
  .version(packageJson.version);

program
  .command('start')
  .description('Guide robots around the perilous surface of Mars')
  .action(() => {
    console.log(chalk.yellow('=========*** Martian Robots ***=========='));
    prompt(getMarsQuestions())
      .then((marsAnswers: MarsAnswers) => {
        const [boundsX, boundsY] = parseBounds(marsAnswers.bounds);
        Mars(boundsX, boundsY);

        askRobotQuestions(marsAnswers);
      });
  });

let numRobots = 0;
const askRobotQuestions = (marsAnswers: MarsAnswers) => {
  numRobots = numRobots + 1;
  console.log(chalk.cyan(`\n=============== Robot ${numRobots} =================`));
  prompt(getRobotQuestions(marsAnswers))
    .then(({ positions, instructions }: RobotAnswers) => {
      const [x, y, orientation] = parseInitialPositions(positions);
      const parsedInstructions = parseInstructions(instructions) as RobotInstruction[];

      const {
        coordinateX: finalX,
        coordinateY: finalY,
        isLost,
        orientation: finalOrientation,
      } = Robot(`robot-${numRobots}`, x, y, orientation as Orientation, Planet.Mars)
        .navigate(parsedInstructions);

      console.log(chalk.yellow(`* `
        + chalk.white.bold(`Robot ${numRobots}'s Final Position: `)
        + `${finalX} ${finalY} ${finalOrientation} `
        + `${isLost ? 'LOST' : ''}`,
      ));
      askRobotQuestions(marsAnswers); // Continue asking robot questions until program is exited
    });
};

program.parse(process.argv);
