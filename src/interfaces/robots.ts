import { LocationData } from './location';

export enum TrackingStatus {
  Known = 'KNOWN',
  Lost = 'LOST',
}

export enum RobotInstruction {
  Forward = 'F',
  Left = 'L',
  Right = 'R',
}

export interface NewRobot extends LocationData {
  id: string;
  isOffGrid?: boolean;
}

export type Robot = NewRobot & {
  isOffGrid: boolean;
};
