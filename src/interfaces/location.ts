import { Planet } from './application';

export enum Orientation {
  North = 'N',
  East = 'E',
  South = 'S',
  West = 'W',
}

export interface LocationData {
  coordinateX: number;
  coordinateY: number;
  orientation: Orientation;
  planetId: Planet;
}

export enum RotationDirection {
  Clockwise = 1,
  CounterClockwise = -1,
}

