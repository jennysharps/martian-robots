import { Planet as PlanetInterface } from './planets';
import { Robot } from './robots';


export enum Species {
  Robot = 'robot',
}

export enum Planet {
  Mars = 'mars',
}

export enum Scent {
  RobotDeath = 'robotDeath',
}

export interface PlanetsState {
  [id: Planet]: PlanetInterface;
}

export interface RobotsState {
  [id: string]: Robot;
}

export interface State {
  planets: PlanetsState;
  robots: RobotsState;
}
