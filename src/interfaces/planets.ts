import { Planet, Scent as KnownScent, Species } from './application';
import { LocationData } from './location';

export interface NewPlanet {
  boundsX: number;
  boundsY: number;
  id: Planet;
  scents?: Scent[];
  visitors?: Visitor[];
}

export type Planet = NewPlanet & {
  scents: Pick<NewPlanet, 'scents'>;
  visitors: Pick<NewPlanet, 'visitors'>;
};

export interface Visitor {
  id: string;
  planetId: Planet;
  species: Species;
}

export interface Scent extends LocationData {
  name: KnownScent;
}

