import { Reducer } from 'redux';
import { RotationDirection } from '../interfaces/location';
import { Species, RobotsState } from '../interfaces/application';

import { ADD_ROBOT, REMOVE_ROBOT } from '../actions/robots';
import { ADD_VISITOR as PLANET_ADD_VISITOR } from '../actions/planets';
import { MOVE_FORWARD, TURN_LEFT, TURN_RIGHT } from '../actions/location';

import { getForwardCoordinates } from '../utils/getForwardCoordinates';
import { getRotatedOrientation } from '../utils/getRotatedOrientation';

const initialState = {};

export const robots: Reducer<RobotsState> = (
  state = initialState,
  { type, robot, navigator, planet, visitor },
) => {
  switch (type) {
    case ADD_ROBOT:
      return {
        ...state,
        [robot.id]: robot,
      };

    case REMOVE_ROBOT:
      return Object.keys(state)
        .reduce((robots, id) => {
          if (id !== robot.id) {
            return {
              ...robots,
              [id]: state[id],
            };
          }
          return robots;
        }, {});

    case MOVE_FORWARD: {
      const { boundsX, boundsY, id, caution, species } = navigator;

      if (species !== Species.Robot) {
        return state;
      }

      const robot = state[id];
      if (!robot || robot.isOffGrid) {
        return state;
      }

      const { coordinateX, coordinateY } = getForwardCoordinates(robot);

      if (
        coordinateX < 0
        || coordinateY < 0
        || coordinateX > boundsX
        || coordinateY > boundsY
      ) {
        if (caution) {
          return state; // Ignore forward instruction
        }

        return {
          ...state,
          [id]: {
            ...state[id],
            isOffGrid: true, // Lost forever
          },
        };
      }

      // Move forward
      return {
        ...state,
        [id]: {
          ...state[id],
          coordinateX,
          coordinateY,
        },
      };
    }

    case TURN_LEFT:
    case TURN_RIGHT: {
      const { id, species } = navigator;
      if (species !== Species.Robot) {
        return state;
      }

      const robot = state[id];
      if (!robot || robot.isOffGrid) {
        return state;
      }

      const { orientation } = robot;
      const direction = (
        type === TURN_LEFT
          ? RotationDirection.CounterClockwise
          : RotationDirection.Clockwise
      );
      return {
        ...state,
        [id]: {
          ...state[id],
          orientation: getRotatedOrientation(orientation, { direction }),
        },
      };
    }

    case PLANET_ADD_VISITOR: {
      const { id: planetId } = planet;
      const { id, species } = visitor;

      if (species !== Species.Robot) {
        return state;
      }

      const { [id]: existingRobot = {} } = state;

      return {
        ...state,
        [id]: {
          ...existingRobot,
          planetId,
        },
      };
    }

    default:
      return state;
  }
};

export default robots;
