import { planets } from './planets';
import { ADD_PLANET, ADD_SCENT, ADD_VISITOR, REMOVE_PLANET } from '../actions/planets'

describe('planets reducers', () => {
  describe('ADD_PLANET', () => {
    it('should add planet to state', () => {
      const initialState = deepFreeze({
        existingPlanet: { id: 'existingPlanet', visitors: [], scents: [] },
      });
      const action = {
        type: ADD_PLANET,
        planet: { id: 'mars', visitors: [], scents: [] },
      };
      expect(planets(initialState, action)).to.deep.equal({
        existingPlanet: { id: 'existingPlanet', visitors: [], scents: [] },
        mars: { id: 'mars', visitors: [], scents: [] },
      });
    });
  });

  describe('REMOVE_PLANET', () => {
    it('should remove planet from state', () => {
      const initialState = deepFreeze({
        existingPlanet: { id: 'existingPlanet', visitors: [], scents: [] },
        mars: { id: 'mars', visitors: [], scents: [] },
      });
      const action = {
        type: REMOVE_PLANET,
        planet: { id: 'existingPlanet' },
      };
      expect(planets(initialState, action)).to.deep.equal({
        mars: { id: 'mars', visitors: [], scents: [] },
      });
    });
  });

  describe('ADD_VISITOR', () => {
    it('should add visitor to planet', () => {
      const initialState = deepFreeze({
        existingPlanet: {
          id: 'existingPlanet',
          visitors: [{ id: 'existingVisitor' }],
          scents: [],
        },
      });
      const action = {
        type: ADD_VISITOR,
        planet: { id: 'existingPlanet', visitors: [], scents: [] },
        visitor: { id: 'visitor 2' },
      };
      expect(planets(initialState, action)).to.deep.equal({
        existingPlanet: {
          id: 'existingPlanet',
          visitors: [{ id: 'existingVisitor' }, { id: 'visitor 2' }],
          scents: [],
        },
      });
    });
  });

  describe('ADD_SCENT', () => {
    it('should add scent to planet', () => {
      const initialState = deepFreeze({
        existingPlanet: {
          id: 'existingPlanet',
          visitors: [],
          scents: [{ id: 'existingScent' }],
        },
      });
      const action = {
        type: ADD_SCENT,
        planet: { id: 'existingPlanet', visitors: [], scents: [] },
        scent: { id: 'scent 2' },
      };
      expect(planets(initialState, action)).to.deep.equal({
        existingPlanet: {
          id: 'existingPlanet',
          visitors: [],
          scents: [{ id: 'existingScent' }, { id: 'scent 2' }],
        },
      });
    });
  });

  it('should handle unknown actions', () => {
    const initialState = deepFreeze({});
    const action = {
      type: 'UNKNOWN',
    };
    expect(planets(initialState, action)).to.deep.equal({});
  });
});
