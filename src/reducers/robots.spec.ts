import { robots } from './robots';
import { MOVE_FORWARD, TURN_LEFT, TURN_RIGHT } from '../actions/location';
import { ADD_ROBOT, REMOVE_ROBOT } from '../actions/robots';
import { Species } from '../interfaces/application';
import { Orientation } from '../interfaces/location';

describe('robots reducers', () => {
  describe('ADD_ROBOT', () => {
    it('should add robot to state', () => {
      const initialState = deepFreeze({
        existingRobot: { id: 'existingRobot', isOffGrid: false },
      });
      const action = {
        type: ADD_ROBOT,
        robot: { id: 'robotTwo', isOffGrid: false },
      };
      expect(robots(initialState, action)).to.deep.equal({
        existingRobot: { id: 'existingRobot', isOffGrid: false },
        robotTwo: { id: 'robotTwo', isOffGrid: false },
      });
    });
  });

  describe('REMOVE_ROBOT', () => {
    it('should remove robot from state', () => {
      const initialState = deepFreeze({
        existingRobot: { id: 'existingRobot', isOffGrid: false },
        robotTwo: { id: 'robotTwo', isOffGrid: false },
      });
      const action = {
        type: REMOVE_ROBOT,
        robot: { id: 'existingRobot' },
      };
      expect(robots(initialState, action)).to.deep.equal({
        robotTwo: { id: 'robotTwo', isOffGrid: false },
      });
    });
  });

  describe('MOVE_FORWARD', () => {
    describe('when species is Robot', () => {
      it('should advance robot position 1 space forward', () => {
        const initialState = deepFreeze({
          robotTwo: {
            id: 'robotTwo',
            coordinateX: 1,
            coordinateY: 1,
            isOffGrid: false,
            orientation: Orientation.North,
          },
        });
        const action = {
          type: MOVE_FORWARD,
          navigator: {
            id: 'robotTwo',
            species: Species.Robot,
            boundsX: 5,
            boundsY: 5,
          },
        };
        expect(robots(initialState, action)).to.deep.equal({
          robotTwo: {
            id: 'robotTwo',
            coordinateX: 1,
            coordinateY: 2,
            isOffGrid: false,
            orientation: Orientation.North,
          },
        });
      });
    });

    it('should not change state if robot is offGrid', () => {
      const initialState = deepFreeze({
        robotTwo: {
          id: 'robotTwo',
          coordinateX: 1,
          coordinateY: 1,
          isOffGrid: true,
          orientation: Orientation.North,
        },
      });
      const action = {
        type: MOVE_FORWARD,
        navigator: {
          id: 'robotTwo',
          species: 'something else',
          boundsX: 5,
          boundsY: 5,
        },
      };
      expect(robots(initialState, action)).to.deep.equal(initialState);
    });

    it('should set offGrid to true if robot navigates out of planetary bounds', () => {
      const initialState = deepFreeze({
        robotTwo: {
          id: 'robotTwo',
          coordinateX: 1,
          coordinateY: 5,
          isOffGrid: true,
          orientation: Orientation.North,
        },
      });
      const action = {
        type: MOVE_FORWARD,
        navigator: {
          id: 'robotTwo',
          species: 'something else',
          boundsX: 5,
          boundsY: 5,
        },
      };
      expect(robots(initialState, action)).to.deep.equal({
        robotTwo: {
          id: 'robotTwo',
          coordinateX: 1,
          coordinateY: 5,
          isOffGrid: true,
          orientation: Orientation.North,
        },
      });
    });

    describe('when species is not Robot', () => {
      it('should not change state', () => {
        const initialState = deepFreeze({
          robotTwo: {
            id: 'robotTwo',
            coordinateX: 1,
            coordinateY: 1,
            isOffGrid: false,
            orientation: Orientation.North,
          },
        });
        const action = {
          type: MOVE_FORWARD,
          navigator: {
            id: 'robotTwo',
            species: 'something else',
            boundsX: 5,
            boundsY: 5,
          },
        };
        expect(robots(initialState, action)).to.deep.equal(initialState);
      });
    });
  });

  describe('TURN_RIGHT', () => {
    it('should turn robot 90 degrees clockwise', () => {
      const initialState = deepFreeze({
        robotTwo: {
          id: 'robotTwo',
          coordinateX: 1,
          coordinateY: 1,
          isOffGrid: false,
          orientation: Orientation.North,
        },
      });
      const action = {
        type: TURN_RIGHT,
        navigator: {
          id: 'robotTwo',
          species: Species.Robot,
          boundsX: 5,
          boundsY: 5,
        },
      };
      expect(robots(initialState, action)).to.deep.equal({
        robotTwo: {
          id: 'robotTwo',
          coordinateX: 1,
          coordinateY: 1,
          isOffGrid: false,
          orientation: Orientation.East,
        },
      });
    });
  });

  describe('TURN_LEFT', () => {
    it('should turn robot 90 degrees counterclockwise', () => {
      const initialState = deepFreeze({
        robotTwo: {
          id: 'robotTwo',
          coordinateX: 1,
          coordinateY: 1,
          isOffGrid: false,
          orientation: Orientation.North,
        },
      });
      const action = {
        type: TURN_LEFT,
        navigator: {
          id: 'robotTwo',
          species: Species.Robot,
          boundsX: 5,
          boundsY: 5,
        },
      };
      expect(robots(initialState, action)).to.deep.equal({
        robotTwo: {
          id: 'robotTwo',
          coordinateX: 1,
          coordinateY: 1,
          isOffGrid: false,
          orientation: Orientation.West,
        },
      });
    });
  });

  it('should handle unknown actions', () => {
    const initialState = deepFreeze({});
    const action = {
      type: 'UNKNOWN',
    };
    expect(robots(initialState, action)).to.deep.equal({});
  });
});
