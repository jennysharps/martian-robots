import { Reducer } from 'redux';
import { PlanetsState } from '../interfaces/application';

import { ADD_PLANET, ADD_SCENT, ADD_VISITOR, REMOVE_PLANET } from '../actions/planets';

const initialState = {};

export const planets: Reducer<PlanetsState> = (
  state = initialState,
  { planet, scent, type, visitor },
) => {
  switch (type) {
    case ADD_PLANET:
      return {
        ...state,
        [planet.id]: planet,
      };

    case REMOVE_PLANET:
      return Object.keys(state)
        .reduce((planets, id) => {
          if (id !== planet.id) {
            return {
              ...planets,
              [id]: state[id],
            };
          }
          return planets;
        }, {});

    case ADD_VISITOR: {
      const { [planet.id]: existingPlanet = {} } = state;
      return {
        ...state,
        [planet.id]: {
          ...existingPlanet,
          visitors: [...existingPlanet.visitors, visitor],
        },
      };
    }

    case ADD_SCENT: {
      const { [planet.id]: existingPlanet = {} } = state;
      return {
        ...state,
        [planet.id]: {
          ...existingPlanet,
          scents: [...existingPlanet.scents, scent],
        },
      };
    }

    default:
      return state;
  }
};
