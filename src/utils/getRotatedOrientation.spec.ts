import getRotatedOrientation from './getRotatedOrientation';
import { Orientation, RotationDirection } from '../interfaces/location';

describe('getRotatedOrientation', () => {
  describe('when orientation is North', () => {
    it('should return East by default', () => {
      expect(getRotatedOrientation(Orientation.North)).to.deep.equal(Orientation.East);
    });

    it('should return direction rotated given number of times if amount option is set', () => {
      expect(getRotatedOrientation(Orientation.North, { amount: 2 }))
        .to.deep.equal(Orientation.South);
    });

    it('should return North by default if direction option is set to counterclockwise', () => {
      expect(getRotatedOrientation(
        Orientation.North,
        { direction: RotationDirection.CounterClockwise },
      )).to.deep.equal(Orientation.West);
    });

    it('should return direction rotated given number of times counterclockwise if amount option`'
      + ' is set and if direction option is set to counterclockwise', () => {
      expect(getRotatedOrientation(
        Orientation.North,
        { amount: 2, direction: RotationDirection.CounterClockwise },
      )).to.deep.equal(Orientation.South);
    });
  });

  describe('when orientation is East', () => {
    it('should return South by default', () => {
      expect(getRotatedOrientation(Orientation.East)).to.deep.equal(Orientation.South);
    });

    it('should return direction rotated given number of times if amount option is set', () => {
      expect(getRotatedOrientation(Orientation.East, { amount: 2 }))
        .to.deep.equal(Orientation.West);
    });

    it('should return North by default if direction option is set to counterclockwise', () => {
      expect(getRotatedOrientation(
        Orientation.East,
        { direction: RotationDirection.CounterClockwise },
      )).to.deep.equal(Orientation.North);
    });

    it('should return direction rotated given number of times counterclockwise if amount option`'
      + ' is set and if direction option is set to counterclockwise', () => {
      expect(getRotatedOrientation(
        Orientation.East,
        { amount: 2, direction: RotationDirection.CounterClockwise },
      )).to.deep.equal(Orientation.West);
    });
  });

  describe('when orientation is South', () => {
    it('should return West by default', () => {
      expect(getRotatedOrientation(Orientation.South)).to.deep.equal(Orientation.West);
    });

    it('should return direction rotated given number of times if amount option is set', () => {
      expect(getRotatedOrientation(Orientation.South, { amount: 2 }))
        .to.deep.equal(Orientation.North);
    });

    it('should return East by default if direction option is set to counterclockwise', () => {
      expect(getRotatedOrientation(
        Orientation.South,
        { direction: RotationDirection.CounterClockwise },
      )).to.deep.equal(Orientation.East);
    });

    it('should return direction rotated given number of times counterclockwise if amount option`'
      + ' is set and if direction option is set to counterclockwise', () => {
      expect(getRotatedOrientation(
        Orientation.South,
        { amount: 2, direction: RotationDirection.CounterClockwise },
      )).to.deep.equal(Orientation.North);
    });
  });

  describe('when orientation is West', () => {
    it('should return North by default', () => {
      expect(getRotatedOrientation(Orientation.West)).to.deep.equal(Orientation.North);
    });

    it('should return direction rotated given number of times if amount option is set', () => {
      expect(getRotatedOrientation(Orientation.West, { amount: 2 }))
        .to.deep.equal(Orientation.East);
    });

    it('should return South by default if direction option is set to counterclockwise', () => {
      expect(getRotatedOrientation(
        Orientation.West,
        { direction: RotationDirection.CounterClockwise },
      )).to.deep.equal(Orientation.South);
    });

    it('should return direction rotated given number of times counterclockwise if amount option`'
      + ' is set and if direction option is set to counterclockwise', () => {
      expect(getRotatedOrientation(
        Orientation.West,
        { amount: 2, direction: RotationDirection.CounterClockwise },
      )).to.deep.equal(Orientation.East);
    });
  });
});
