import getForwardCoordinates from './getForwardCoordinates';
import { Orientation } from '../interfaces/location';

describe('getForwardCoordinates', () => {
  describe('when orientation is North', () => {
    it('should return coordinates with 1 added to coordinateY by default', () => {
      expect(getForwardCoordinates({
        coordinateX: 1,
        coordinateY: 1,
        orientation: Orientation.North,
      })).to.deep.equal({ coordinateX: 1, coordinateY: 2 });
    });

    it('should return coordinates amount added to coordinateY if amount is provided', () => {
      expect(getForwardCoordinates(
        { coordinateX: 1, coordinateY: 1, orientation: Orientation.North },
        3,
      )).to.deep.equal({ coordinateX: 1, coordinateY: 4 });
    });
  });

  describe('when orientation is East', () => {
    it('should return coordinates with 1 added to coordinateX by default', () => {
      expect(getForwardCoordinates({
        coordinateX: 1,
        coordinateY: 1,
        orientation: Orientation.East,
      })).to.deep.equal({ coordinateX: 2, coordinateY: 1 });
    });

    it('should return coordinates amount added to coordinateX if amount is provided', () => {
      expect(getForwardCoordinates(
        { coordinateX: 1, coordinateY: 1, orientation: Orientation.East },
        3,
      )).to.deep.equal({ coordinateX: 4, coordinateY: 1 });
    });
  });

  describe('when orientation is South', () => {
    it('should return coordinates with 1 subtracted from coordinateY by default', () => {
      expect(getForwardCoordinates({
        coordinateX: 1,
        coordinateY: 1,
        orientation: Orientation.South,
      })).to.deep.equal({ coordinateX: 1, coordinateY: 0 });
    });

    it('should return coordinates amount subtracted from coordinateY if amount is provided', () => {
      expect(getForwardCoordinates(
        { coordinateX: 1, coordinateY: 1, orientation: Orientation.South },
        3,
      )).to.deep.equal({ coordinateX: 1, coordinateY: -2 });
    });
  });

  describe('when orientation is West', () => {
    it('should return coordinates with 1 subtracted from coordinateX by default', () => {
      expect(getForwardCoordinates({
        coordinateX: 1,
        coordinateY: 1,
        orientation: Orientation.West,
      })).to.deep.equal({ coordinateX: 0, coordinateY: 1 });
    });

    it('should return coordinates amount subtracted from coordinateX if amount is provided', () => {
      expect(getForwardCoordinates(
        { coordinateX: 1, coordinateY: 1, orientation: Orientation.West },
        3,
      )).to.deep.equal({ coordinateX: -2, coordinateY: 1 });
    });
  });
});
