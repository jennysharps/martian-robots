import { parseBounds, parseInitialPositions, parseInstructions } from './parsers';

describe('parsers', () => {
  describe('parseBounds', () => {
    it('should return an array of two numbers given a valid string', () => {
      expect(parseBounds('12 14')).to.deep.equal([12, 14]);
      expect(parseBounds('1 3')).to.deep.equal([1, 3]);
    });

    it('should handle an string without numbers', () => {
      expect(parseBounds('abdc sdfd dfre')).to.deep.equal([NaN, NaN]);
    });
  });

  describe('parseInitialPositions', () => {
    it('should return an array of two numbers and string', () => {
      expect(parseInitialPositions('12 14 abcdefg')).to.deep.equal([12, 14, 'abcdefg']);
      expect(parseInitialPositions('1 3 N')).to.deep.equal([1, 3, 'N']);
    });

    it('should handle an string without numbers', () => {
      expect(parseInitialPositions('abdc efgh ijkl')).to.deep.equal([NaN, NaN, 'ijkl']);
    });
  });

  describe('parseInstructions', () => {
    it('should return an array of all characters in given string', () => {
      expect(parseInstructions('ABCDEFG')).to.deep.equal(['A', 'B', 'C', 'D', 'E', 'F', 'G']);
      expect(parseInstructions('1 3 N')).to.deep.equal(['1', ' ', '3', ' ', 'N']);
    });
  });
});
