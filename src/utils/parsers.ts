type PlanetBounds = [number, number];

type RobotPositions = [number, number, string];

export const parseBounds = (dimensions = ''): PlanetBounds => {
  const [boundsX, boundsY] = dimensions.split(' ');
  return [
    parseInt(boundsX, 10),
    parseInt(boundsY, 10),
  ];
};

export const parseInitialPositions = (initialPositions = ''): RobotPositions => {
  const [x, y, orientation] = initialPositions.split(' ');
  return [
    parseInt(x, 10),
    parseInt(y, 10),
    orientation,
  ];
};

export const parseInstructions = (instructions = ''): string[] =>
  instructions.split('');
