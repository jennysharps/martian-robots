import { Orientation, RotationDirection } from '../interfaces/location';

const orientations = Object.keys(Orientation)
  .map(key => Orientation[key as keyof typeof Orientation]);

export interface RotationOptions {
  direction?: RotationDirection;
  amount?: number;
}

export const getRotatedOrientation = (
  orientation: Orientation,
  {
    direction = RotationDirection.Clockwise,
    amount = 1,
  }: RotationOptions = {},
): Orientation => {
  if (amount === 0) {
    return orientation;
  }

  let orientationIndex = orientations.indexOf(orientation);
  if (direction === RotationDirection.CounterClockwise) {
    // counter clockwise
    orientationIndex = orientationIndex !== 0
      ? orientationIndex - 1 // set to previous
      : orientations.length - 1; // wrap to last
  } else {
    // clockwise
    orientationIndex = orientationIndex !== orientations.length - 1
      ? orientationIndex + 1 // set to next
      : 0; // wrap to first
  }

  if (amount > 1) {
    // continue rotating until no rotations left to process
    return getRotatedOrientation(
      orientations[orientationIndex],
      {
        direction,
        amount: amount - 1,
      },
    );
  }

  return orientations[orientationIndex];
};

export default getRotatedOrientation;
