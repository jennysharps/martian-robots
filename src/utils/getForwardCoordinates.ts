import { LocationData, Orientation } from '../interfaces/location';

export const getForwardCoordinates = (
  { coordinateX, coordinateY, orientation }: LocationData,
  amount: number = 1,
): { x: number, y: number }  => {
  let x = coordinateX;
  let y = coordinateY;

  switch (orientation) {
    case Orientation.North: y = coordinateY + amount; break;
    case Orientation.East:  x = coordinateX + amount; break;
    case Orientation.South: y = coordinateY - amount; break;
    case Orientation.West:  x = coordinateX - amount; break;
  }

  return { coordinateX: x, coordinateY: y };
};

export default getForwardCoordinates;
