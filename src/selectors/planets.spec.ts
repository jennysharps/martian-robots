import { getPlanet, getPlanetBounds, getScentsAtLocation } from './planets';

describe('planets selectors', () => {
  describe('getPlanet', () => {
    it('should return requested planet given state and id', () => {
      const store = {
        planets: {
          mars: { id: 'mars' },
          pluto: { id: 'pluto' },
        },
      };
      expect(getPlanet(store, 'mars')).to.deep.equal({ id: 'mars' });
    });
  });

  describe('getPlanetBounds', () => {
    it('should return requested planet\'s bounds given state and id', () => {
      const store = {
        planets: {
          mars: { id: 'mars', boundsX: 2, boundsY: 3 },
          pluto: { id: 'pluto', boundsX: 4, boundsY: 5  },
        },
      };
      expect(getPlanetBounds(store, 'mars')).to.deep.equal({ boundsX: 2, boundsY: 3 });
    });
  });

  describe('getScentsAtLocation', () => {
    it('should return requested planet\'s scents given state, id, and coordinates', () => {
      const store = {
        planets: {
          earth: { id: 'pluto', boundsX: 2, boundsY: 3, scents: []  },
          mars: { id: 'mars', boundsX: 2, boundsY: 3, scents: ['scent 1', 'scent 2'] },
          pluto: { id: 'pluto', boundsX: 4, boundsY: 5, scents: []  },
        },
      };
      expect(getScentsAtLocation(store, 'mars', 2, 3)).to.deep.equal(['scent 1', 'scent 2']);
    });
  });
});
