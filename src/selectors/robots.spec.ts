import { getRobot } from './robots';

describe('robots selectors', () => {
  describe('getRobot', () => {
    it('should return requested robot given state and id', () => {
      const store = {
        robots: {
          robotOne: { id: 'robotOne' },
          robotTwo: { id: 'robotTwo' },
        },
      };
      expect(getRobot(store, 'robotTwo')).to.deep.equal({ id: 'robotTwo' });
    });
  });
});
