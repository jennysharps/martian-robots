import { Robot } from '../interfaces/robots';
import { State } from '../interfaces/application';

export const getRobot = ({ robots }: State, id: string): Robot => robots[id];
