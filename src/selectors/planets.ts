import { Planet, Visitor, Scent } from '../interfaces/planets';
import { Planet as KnownPlanet, Scent as KnownScent, State } from '../interfaces/application';

export const getPlanet = ({ planets }: State, id: KnownPlanet): Planet => planets[id];

export const getPlanetBounds = ({ planets }: State, id: KnownPlanet): Planet => {
  const { boundsX, boundsY } = planets[id] || { boundsX: 0, boundsY: 0 };

  return {
    boundsX,
    boundsY,
  };
};

export const getScentsAtLocation = (
  { planets }: State,
  id: KnownPlanet,
  coordinatesX,
  coordinatesY,
): Scent[] => {
  const { scents = [] } = planets[id] || {};
  return scents.reduce((matchingScents, scent) => {
    const { coordinatesX: x, coordinatesY: y } = scent;
    return x !== coordinatesX && y !== coordinatesY
      ? [...matchingScents, scent]
      : matchingScents;
  }, []);
};
