import { ThunkAction } from 'redux-thunk';
import { Planet, Species } from '../interfaces/application';
import { State } from '../reducers/index';

import { getPlanet, getPlanetBounds, getScentsAtLocation } from '../selectors/planets'

const namespace = 'LOCATION';
export const TURN_LEFT = `${namespace}/TURN_LEFT`;
export const TURN_RIGHT = `${namespace}/TURN_RIGHT`;
export const MOVE_FORWARD = `${namespace}/MOVE_FORWARD`;

type LocationActionCreator = (
  id: string,
  species: Species,
  planetId: Planet,
) => ThunkAction<void, State, void>;

export const moveForward: LocationActionCreator = (id, species, planetId, options = {}) =>
  (dispatch, getState) => {
    dispatch({
      type: MOVE_FORWARD,
      navigator: {
        id,
        species,
        ...options,
        ...getPlanetBounds(getState(), planetId),
      },
    });
  };

export const turnLeft: LocationActionCreator = (id, species, planetId) =>
  (dispatch, getState) => {
    dispatch({
      type: TURN_LEFT,
      navigator: {
        id,
        species,
        ...getPlanetBounds(getState(), planetId),
      },
    });
  };

export const turnRight: LocationActionCreator = (id, species, planetId) =>
  (dispatch, getState) => {
    dispatch({
      type: TURN_RIGHT,
      navigator: {
        id,
        species,
        ...getPlanetBounds(getState(), planetId),
      },
    });
  };
