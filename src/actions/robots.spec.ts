import {
  ADD_ROBOT,
  createRobot,
  destroyRobot,
  REMOVE_ROBOT,
} from './robots';

describe('robots actions', () => {
  describe('createRobot', () => {
    it('should return action type ADD_ROBOT and correct data', () => {
      expect(createRobot({ id: 'robotId', two: 'dataTwo' })).to.deep.equal({
        type: ADD_ROBOT,
        robot: {
          id: 'robotId',
          isOffGrid: false,
          two: 'dataTwo',
        },
      });
    });
  });

  describe('destroyRobot', () => {
    it('should return action type REMOVE_ROBOT and correct data', () => {
      expect(destroyRobot('robotId')).to.deep.equal({
        type: REMOVE_ROBOT,
        robot: { id: 'robotId' },
      });
    });
  });
});
