import { AnyAction } from 'redux';
import { NewPlanet, Scent, Visitor } from '../interfaces/planets';

const namespace = 'PLANETS';
export const ADD_PLANET = `${namespace}/ADD_PLANET`;
export const REMOVE_PLANET = `${namespace}/REMOVE_PLANET`;
export const ADD_VISITOR = `${namespace}/ADD_VISITOR`;
export const ADD_SCENT = `${namespace}/ADD_SCENT`;

export const createPlanet = ({
  scents = [],
  visitors = [],
 ...restPlanet,
}: NewPlanet): AnyAction => ({
  type: ADD_PLANET,
  planet: {
    scents,
    visitors,
    ...restPlanet,
  },
});

export const destroyPlanet = (planetId: string): AnyAction => ({
  type: REMOVE_PLANET,
  planet: { id: planetId },
});

export const addVisitor = ({ planetId, ...visitor }: Visitor): AnyAction => ({
  type: ADD_VISITOR,
  planet: { id: planetId },
  visitor,
});

export const leaveScent = ({ planetId, ...scent }: Scent): AnyAction => ({
  type: ADD_SCENT,
  planet: { id: planetId },
  scent,
});

