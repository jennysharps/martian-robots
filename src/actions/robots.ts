import { AnyAction } from 'redux';
import { NewRobot } from '../interfaces/robots';

const namespace = 'ROBOTS';
export const ADD_ROBOT = `${namespace}/ADD_ROBOT`;
export const REMOVE_ROBOT = `${namespace}/REMOVE_ROBOT`;

export const createRobot = ({
  isOffGrid = false,
  ...robot,
}: NewRobot): AnyAction => ({
  type: ADD_ROBOT,
  robot: {
    ...robot,
    isOffGrid,
  },
});

export const destroyRobot = (robotId: string): AnyAction => ({
  type: REMOVE_ROBOT,
  robot: { id: robotId },
});
