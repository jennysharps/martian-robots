import {
  MOVE_FORWARD,
  moveForward,
  TURN_LEFT,
  TURN_RIGHT,
  turnLeft,
  turnRight,
} from './location';

describe('location actions', () => {
  let store;

  beforeEach(() => {
    store = mockStore({
      planets: {
        mars: {
          boundsX: 'marsXBounds',
          boundsY: 'marsYBounds',
        },
      },
    });
  });

  describe('moveForward', () => {
    it('should dispatch action with type MOVE_FORWARD and correct data', () => {
      store.dispatch(moveForward('navigatorId', 'navigatorSpecies', 'mars'));

      expect(store.getActions()).to.deep.equal([{
        type: MOVE_FORWARD,
        navigator: {
          boundsX: 'marsXBounds',
          boundsY: 'marsYBounds',
          id: 'navigatorId',
          species: 'navigatorSpecies',
        },
      }]);
    });
  });

  describe('turnLeft', () => {
    it('should dispatch action with type TURN_LEFT and correct data', () => {
      store.dispatch(turnLeft('navigatorId', 'navigatorSpecies', 'mars'));

      expect(store.getActions()).to.deep.equal([{
        type: TURN_LEFT,
        navigator: {
          boundsX: 'marsXBounds',
          boundsY: 'marsYBounds',
          id: 'navigatorId',
          species: 'navigatorSpecies',
        },
      }]);
    });
  });

  describe('turnRight', () => {
    it('should dispatch action with type TURN_RIGHT and correct data', () => {
      store.dispatch(turnRight('navigatorId', 'navigatorSpecies', 'mars'));

      expect(store.getActions()).to.deep.equal([{
        type: TURN_RIGHT,
        navigator: {
          boundsX: 'marsXBounds',
          boundsY: 'marsYBounds',
          id: 'navigatorId',
          species: 'navigatorSpecies',
        },
      }]);
    });
  });
});
