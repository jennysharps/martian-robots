import {
  ADD_PLANET,
  ADD_SCENT,
  ADD_VISITOR,
  addVisitor,
  createPlanet,
  destroyPlanet,
  leaveScent,
  REMOVE_PLANET,
} from './planets';

describe('planets actions', () => {
  describe('createPlanet', () => {
    it('should return action type ADD_PLANET and correct data', () => {
      expect(createPlanet({ one: 'dataOne', two: 'dataTwo' })).to.deep.equal({
        type: ADD_PLANET,
        planet: {
          one: 'dataOne',
          two: 'dataTwo',
          scents: [],
          visitors: [],
        },
      });
    });
  });

  describe('destroyPlanet', () => {
    it('should return action type REMOVE_PLANET and correct data', () => {
      expect(destroyPlanet('mars')).to.deep.equal({
        type: REMOVE_PLANET,
        planet: { id: 'mars' },
      });
    });
  });

  describe('addVisitor', () => {
    it('should return action type ADD_VISITOR and correct data', () => {
      expect(addVisitor({ planetId: 'mars', two: 'dataTwo' })).to.deep.equal({
        type: ADD_VISITOR,
        planet: { id: 'mars' },
        visitor: { two: 'dataTwo' },
      });
    });
  });

  describe('leaveScent', () => {
    it('should return action type ADD_SCENT and correct data', () => {
      expect(leaveScent({ planetId: 'mars', two: 'dataTwo' })).to.deep.equal({
        type: ADD_SCENT,
        planet: { id: 'mars' },
        scent: { two: 'dataTwo' },
      });
    });
  });
});
