import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import { planets } from '../reducers/planets';
import { robots } from '../reducers/robots';

import { State } from '../interfaces/application';

export const store = createStore(
  combineReducers<State>({
    planets,
    robots,
  }),
  applyMiddleware(thunk),
);

export default store;
