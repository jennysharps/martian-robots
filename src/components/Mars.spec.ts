import Mars from './Mars';

describe('Mars', () => {
  it('should initiate with correct properties', () => {
    const mars = Mars(2, 3);
    expect(mars).to.deep.contain({
      boundsX: 2,
      boundsY: 3,
    });
  });
});
