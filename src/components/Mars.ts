// Interfaces
import { Visitor } from '../interfaces/planets';

// State
import { store } from '../store';
import { getPlanet } from '../selectors/planets';

// Actions
import { createPlanet, destroyPlanet } from '../actions/planets';

interface MarsState {
  boundsX: number;
  boundsY: number;
  destroy: () => void;
}

export const Mars = (boundsX: number, boundsY: number): MarsState => {
  const init = () => {
    store.dispatch(createPlanet({ boundsX, boundsY, id: 'mars' })); // load Mars
    syncState();
  };

  const syncState = () => {
    const {
      boundsX = state.boundsX,
      boundsY = state.boundsY,
    } = getPlanet(store.getState(), 'mars');
    state.boundsX = boundsX;
    state.boundsY = boundsY;
  };

  // Subscribe to store updates and capture unsubscribe callback
  const unsubscribe = store.subscribe(syncState);

  const destroy = () => {
    unsubscribe();
    store.dispatch(destroyPlanet('mars'));
  };

  const state: MarsState = {
    boundsX,
    boundsY,
    destroy,
  };

  init();

  return state;
};

export default Mars;
