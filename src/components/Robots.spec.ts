import Robot, { navigationActionMap } from './Robot';
import { Orientation } from '../interfaces/location';

describe('Robots', () => {
  it('should initiate with correct properties', () => {
    const robot = Robot('robot-1', 3, 4, Orientation.South, 'mars');
    expect(robot).to.deep.contain({
      coordinateX: 3,
      coordinateY: 4,
      deathScentDetected: false,
      isLost: false,
      orientation: Orientation.South,
      planetId: 'mars',
    });
  });

  describe('navigate', () => {
    it('should call a navigation action once per navigation instruction', () => {
      const navigationActionF = sinon.spy(navigationActionMap, 'F');
      const navigationActionR = sinon.spy(navigationActionMap, 'R');
      const navigationActionL = sinon.spy(navigationActionMap, 'L');
      const instructions = ['F', 'R', 'R', 'R', 'L', 'F'];

      Robot('robot-1', 1, 1, Orientation.South, 'mars')
        .navigate(instructions);

      expect(navigationActionF).to.have.been.calledTwice;
      expect(navigationActionR).to.have.been.calledThrice;
      expect(navigationActionL).to.have.been.calledOnce;

      navigationActionF.restore();
      navigationActionR.restore();
      navigationActionL.restore();
    });

    it('should not call navigation actions if robot is lost', () => {
      const navigationActionF = sinon.spy(navigationActionMap, 'F');
      const navigationActionR = sinon.spy(navigationActionMap, 'R');
      const navigationActionL = sinon.spy(navigationActionMap, 'L');

      const robot = Robot('robot-1', 1, 1, Orientation.South, 'mars');
      robot.isLost = true;
      robot.navigate(['F', 'R', 'R', 'R', 'L', 'F']);

      expect(navigationActionF).to.have.not.been.called;
      expect(navigationActionR).to.have.not.been.called;
      expect(navigationActionL).to.have.not.been.called;

      navigationActionF.restore();
      navigationActionR.restore();
      navigationActionL.restore();
    });

    it('should call navigation actions with caution if death scent is detected', () => {
      const navigationActionF = sinon.spy(navigationActionMap, 'F');
      const navigationActionR = sinon.spy(navigationActionMap, 'R');
      const navigationActionL = sinon.spy(navigationActionMap, 'L');

      const robot = Robot('robot-1', 1, 1, Orientation.South, 'mars');
      robot.deathScentDetected = true;
      robot.navigate(['F']);
      robot.deathScentDetected = true;
      robot.navigate(['R']);
      robot.deathScentDetected = true;
      robot.navigate(['L']);

      expect(navigationActionF.firstCall.args[3]).to.deep.equal({ caution: true });
      expect(navigationActionR.firstCall.args[3]).to.deep.equal({ caution: true });
      expect(navigationActionL.firstCall.args[3]).to.deep.equal({ caution: true });

      navigationActionF.restore();
      navigationActionR.restore();
      navigationActionL.restore();
    });
  });
});
