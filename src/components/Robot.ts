// Interfaces
import { Planet, Scent, Species } from '../interfaces/application';
import { Orientation } from '../interfaces/location';
import { RobotInstruction } from '../interfaces/robots';

// State
import { store } from '../store';
import { getRobot } from '../selectors/robots';

// Actions
import { createRobot, destroyRobot } from '../actions/robots';
import { moveForward, turnLeft, turnRight } from '../actions/location';
import { addVisitor, leaveScent } from '../actions/planets';
import { getScentsAtLocation } from '../selectors/planets';

const species = Species.Robot;

export const navigationActionMap = {
  [RobotInstruction.Forward]: moveForward,
  [RobotInstruction.Left]: turnLeft,
  [RobotInstruction.Right]: turnRight,
};

interface RobotState {
  coordinateX: number;
  coordinateY: number;
  deathScentDetected: boolean;
  destroy: () => void;
  isLost: boolean;
  navigate: (instructions: RobotInstruction[]) => RobotState;
  orientation: Orientation;
  planetId: string;
}

const isDeathScentDetected = ({ planetId, coordinateX, coordinateY }: RobotState) => (
  getScentsAtLocation(store.getState(), planetId, coordinateX, coordinateY)
    .some(({ name }) => name === Scent.RobotDeath)
);

export const Robot = (
  id: string,
  coordinateX: number,
  coordinateY: number,
  orientation: Orientation,
  planetId: Planet,
): RobotState => {
  const init = () => {
    store.dispatch(createRobot({ id, coordinateX, coordinateY, orientation })); // load robot
    store.dispatch(addVisitor({ id, planetId, species })); // load robot onto planet
    syncState();
  };

  const syncState = () => {
    const {
      coordinateX = state.coordinateX,
      coordinateY = state.coordinateY,
      isOffGrid = state.isLost,
      orientation = state.orientation,
      planetId = state.planetId,
    } = getRobot(store.getState(), id) || {};

    state.coordinateX = coordinateX;
    state.coordinateY = coordinateY;
    state.isLost = isOffGrid;
    state.orientation = orientation;
    state.planetId = planetId;
    state.deathScentDetected = isDeathScentDetected(state);
  };

  // Subscribe to store updates and capture unsubscribe callback
  const unsubscribe = store.subscribe(syncState);

  const destroy = () => {
    unsubscribe();
    store.dispatch(destroyRobot(id));
  };

  const navigate = (instructions: RobotInstruction[]) => {
    instructions.forEach((instruction) => {
      if (state.isLost) {
        return state;
      }

      if (!navigationActionMap[instruction]) {
        throw new Error(`No action found for instruction "${instruction}"`);
      }

      store.dispatch(navigationActionMap[instruction](
        id,
        species,
        state.planetId,
        { caution: state.deathScentDetected },
      ));

      if (state.isLost) {
        store.dispatch(leaveScent({
          coordinateX: state.coordinateX,
          coordinateY: state.coordinateY,
          name: Scent.RobotDeath,
          planetId: state.planetId,
        }));
      }
    });

    return state;
  };

  const state: RobotState = {
    coordinateX,
    coordinateY,
    deathScentDetected: false,
    destroy,
    isLost: false,
    navigate,
    orientation,
    planetId,
  };

  init();

  return state;
};

export default Robot;
